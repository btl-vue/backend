<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arrayEmail = ["canhnnt@thefirstone.jp", "conghc@thefirstone.jp", "hoangnd@thefirstone.jp", "haihv@thefirstone.jp", "vukt@thefirstone.jp", "lienntk@thefirstone.jp", "longdh@thefirstone.jp", "lyvtk@thefirstone.jp", "anhlt@thefirstone.jp", "tranglt@thefirstone.jp", "sonlt@thefirstone.jp", "anhlxt@thefirstone.jp", "linhltt@thefirstone.jp", "tuonglv@thefirstone.jp", "duclh@thefirstone.jp", "dunglt@thefirstone.jp", "trinh@thefirstone.jp", "vinhnt@thefirstone.jp", "huentb@thefirstone.jp", "lynth@thefirstone.jp", "giangntt@thefirstone.jp", "kiennt@thefirstone.jp", "huynv@thefirstone.jp", "haind@thefirstone.jp", "hiennt@thefirstone.jp", "tieppd@thefirstone.jp", "hangpt@thefirstone.jp", "anhpt@thefirstone.jp", "hieppt@thefirstone.jp", "dopv@thefirstone.jp", "thaottp@thefirstone.jp", "hungt@thefirstone.jp", "trangnt@thefirstone.jp", "huongtl@thefirstone.jp", "phuongtt@thefirstone.jp", "dinhtt@thefirstone.jp", "vantt@thefirstone.jp", "tuyetnt@thefirstone.jp", "tuvd@thefirstone.jp", "hoavnk@thefirstone.jp", "hongvt@thefirstone.jp", "thachvd@thefirstone.jp", "sononp@thefirstone.jp", "lapdd@thefirstone.jp", "tuandt@thefirstone.jp"];
        foreach ($arrayEmail as $email){
            DB::table('users')->insert([
                "name" => strtok($email, '@'),
                "email" => $email,
                "password" => Hash::make(strtok($email, '@')."@tfo"),
                "department_id" => 1,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ]);
        }
    }
}
