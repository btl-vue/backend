<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name', 191);
            $table->string('email', 191)->unique();
            $table->string('password', 191);
            $table->string('phone', 191)->nullable();
            $table->tinyInteger('role')->default(2)->comment('1: Admin , 2: User');;
            $table->tinyInteger('department_id')->default(1)->comment('Phòng SD');;
            $table->string('status')->default(0)->comment('0: Đang làm , 1: Đã nghỉ việc');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
