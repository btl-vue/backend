<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name',191);
            $table->unsignedBigInteger('category_id');
            $table->string('code_producer',191)->nullable()->unique();
            $table->string('code_tfo',191)->nullable()->unique();
            $table->text('description')->nullable();
            $table->tinyInteger('type')->default(1)->comment('1: Cần trả , 2: Không cần trả');
            $table->date('guarantee_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
};
