## Kiến trúc cơ bản
- Laravel: 9.0.
- Database: Mysql v5.7
## Hướng dẫn clone dự án
- git clone https://gitlab.com/btl-vue/backend.git
## Setup dự án trên local
- Yêu cầu PHP 8.0 trở lên
### Setup file .env
- setup lại tên database
- setup **APP_URL** là tên domain của web
### Setup dự án
- run **composer install**
- run **php artisan key:generate**
- run **php artisan migrate**
- run **php artisan db:seed --class=DepartmentSeeder**
- run **php artisan db:seed --class=UserAccountSeeder**
- run **php artisan serve**
## Tài khoản
- email, username sẽ là tên viết tắt của bạn vd: Phạm Thế Anh => username: Anhpt, email: anhpt@thefirstone.jp, password anhpt@tfo
