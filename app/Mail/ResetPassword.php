<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\User;

class ResetPassword extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $user;
    private $password;
    public function __construct(User $user, $password)
    {
        $this->user= $user;
        $this->password= $password;
        $this->queue = "email";
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
        ->subject("Send new Password")
        ->view('mail.reset_password', [
            'user' => $this->user,
            'password' => $this->password,
        ]);
    }
}
