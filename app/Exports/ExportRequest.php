<?php

namespace App\Exports;

use App\Models\RequestModel;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Cell\DataValidation;
use Illuminate\Http\Request;

class ExportRequest implements FromCollection, WithHeadings, WithEvents, WithStrictNullComparison
{
    private $request;
    protected $results;

    public function __construct()
    {
        $this->request = request();
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $start_date = isset($this->request->date) ? date("Y-m-d", strtotime($this->request->date[0])) :  "";
        $end_date = isset($this->request->date) ? date("Y-m-d", strtotime($this->request->date[1])) : "";

        $request = RequestModel::with(['user', 'category']);
        if($start_date && $end_date)
        {
            $request->where('date_start', ">=" ,$start_date)->where('date_end', "<=" ,$end_date);
        }
        if ($this->request->category != 0){
            $request->where('category_id', $this->request->category);
        }
        if ($this->request->status != ""){
            $request->where('status', $this->request->status);
        }
        $datas = $request->get();
        $exportRequest = [];
        if(!empty($datas)){
            foreach ($datas as $row) {
                if ($row->status == REQUESTED){
                    $status = "Chờ duyệt";
                }else if ($row->status == REJECTED){
                    $status = "Từ chối";
                }else if ($row->status == RETURNTED){
                    $status = "Đã trả";
                }else{
                    $status = "Đã duyệt";
                }
                $exportRequest[] = array(
                    '0' => $row->id,
                    '1' => $row->user->name,
                    '2' => $row->category->name,
                    '3' => $row->reason,
                    '4' => $row->message,
                    '5' => $row->quantity,
                    '6' => $status,
                    '7' => $row->date_start,
                    '8' => $row->date_end
                );
            }
        }
        $this->results = $exportRequest;
        return (collect($exportRequest));
    }

    public function headings(): array
    {
        return [
            "ID",
            "Tên người mượn",
            "Tên thiết bị",
            "Lý do",
            "Lý trả lại",
            "Số lượng",
            "Trạng thái",
            "Ngày bắt đầu mượn",
            "Ngày trả"
        ];
    }

    public function registerEvents(): array
    {
        return [
            // handle by a closure.
            AfterSheet::class => function(AfterSheet $event) {

                // get layout counts (add 1 to rows for heading row)
                $row_count = 1000;
                $column_count = 1000;

                // set dropdown column
                $drop_column = 'G';

                // set dropdown options
                $options = [
                    'Từ chối',
                    'Đã duyệt',
                ];

                // set dropdown list for first data row
                $validation = $event->sheet->getCell("{$drop_column}2")->getDataValidation();
                $validation->setType(DataValidation::TYPE_LIST );
                $validation->setErrorStyle(DataValidation::STYLE_INFORMATION );
                $validation->setAllowBlank(false);
                $validation->setShowInputMessage(true);
                $validation->setShowErrorMessage(true);
                $validation->setShowDropDown(true);
                $validation->setErrorTitle('Input error');
                $validation->setError('Value is not in list.');
                $validation->setPromptTitle('Pick from list');
                $validation->setPrompt('Please pick a value from the drop-down list.');
                $validation->setFormula1(sprintf('"%s"',implode(',',$options)));

                // clone validation to remaining rows
                for ($i = 3; $i <= $row_count; $i++) {
                    $event->sheet->getCell("{$drop_column}{$i}")->setDataValidation(clone $validation);
                }

                // set columns to autosize
                for ($i = 1; $i <= $column_count; $i++) {
                    $column = Coordinate::stringFromColumnIndex($i);
                    $event->sheet->getColumnDimension($column)->setAutoSize(true);
                }
            },
        ];
    }
}
