<?php

namespace App\Imports;

use App\Models\RequestModel;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class UpdateRequestImportExcel implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        foreach ($collection as $key => $row)
        {
            if ($key > 0){
                if ($row[6] == "Chờ duyệt"){
                    $status = REQUESTED;
                }else if ($row[6] == "Từ chối"){
                    $status = REJECTED;
                }else{
                    $status = APPROVED;
                }
                RequestModel::find($row[0])->update([
                    'status' => $status,
                    'message' => $status == REJECTED ? $row[4] : null
                ]);
            }
        }
    }
}
