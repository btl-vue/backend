<?php

namespace App\Imports;

use App\Models\ProductModel;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ProductsImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new ProductModel([
            'name'     => $row["name"],
            'category_id'    => $row["category_id"],
            'code_producer' => $row["code_producer"],
            'code_tfo' => $row["code_tfo"],
            'description' => $row["description"]
        ]);
    }
}
