<?php

namespace App\Http\Controllers;

use App\Models\CategoryModel;
use App\Models\DepartmentModel;
use Illuminate\Http\Request;
use App\Models\User;


class HelperController extends Controller
{
    private $categoryModel;
    private $department;
    public function __construct(CategoryModel $categoryModel, DepartmentModel $department)
    {
        $this->categoryModel = $categoryModel;
        $this->department = $department;
    }
    public function products()
    {
        $types= [
            'Đang sử dụng'=> PRODUCT_ON,
            'Sản phẩm lỗi'=> PRODUCT_OFF,
       ];
        return response()->json([
            'categories' => $this->categoryModel->select('id', 'name')->get()->pluck('id', 'name'),
            'types' => $types
        ]);
    }

    public function users()
    {
        $roles= [
            'Nhân viên'=> USER,
            'Thủ kho'=> DIVISON_MANAGER,
            'Trưởng phòng (Divison manager)'=> STORE_KEEPER
        ];

        $status= [
             'Đang làm việc'=> USER_ON,
             'Đã nghỉ'=> USER_OFF,
        ];
        return response()->json([
             'departments' => $this->department->select('id', 'name')->get()->pluck('id', 'name'),
             'roles' => $roles,
             'status' => $status
         ]);
    }

    public function requests()
    {
        return response()->json([
            'categories' => $this->categoryModel->select('id', 'name')->get()->pluck('id', 'name'),
        ]);
    }
}
