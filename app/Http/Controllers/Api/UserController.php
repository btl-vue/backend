<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserEditProfileRequest;
use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    private $user;
    public function __construct(User $user)
    {
        $this->user = $user;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = UserResource::collection($this->user->list());
        $response = [
            'pagination' => [
                'total' => $datas->total(),
                'per_page' => $datas->perPage(),
                'current_page' => $datas->currentPage(),
                'last_page' => $datas->lastPage(),
                'from' => $datas->firstItem(),
                'to' => $datas->lastItem()
            ],
            'dataAll' => $datas,
        ];
        return response()->json($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserStoreRequest $request)
    {
        $this->user->createUser($request);
        return response()->json([
            'message' => CREATE_SUCCESS
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user= new UserResource($this->user->find($id));
        return response()->json($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // UserUpdateRequest
    public function update(UserUpdateRequest $request, $id)
    {
        $this->user->updateUser($request, $id);
        return response()->json(['message' => UPDATE_SUCCESS]);
    }

    public function updateProfile(UserEditProfileRequest $request)
    {
        try {
            $this->user->find($request->id)->update([
                "phone" => $request->phone
            ]);
        }catch (\Exception){
            return response()->json(['message' => UPDATE_ERROR], 500);
        }
        return response()->json(['message' => UPDATE_SUCCESS], 200);
    }

    public function resetPassword($id)
    {
        $this->user->resetPassword($id);
        return response()->json([
            'status' => 'success',
            'message' => "Reset password thành công"
        ]);
    }

    public function forgotPassword(Request $request)
    {
        $checkSuccess = $this->user->forgotPassword($request->email);
        return response()->json([
            $checkSuccess
        ]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($this->user->deleteUser($id)) {
            return response()->json(['status' => true, 'message' => DELETE_SUCCESS]);
           }
           return response()->json(['status' => false, 'message' => DELETE_ERROR]);
    }
}
