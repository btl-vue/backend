<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductStoreRequest;
use App\Http\Requests\ProductUpdateRequest;
use App\Http\Resources\ProductResource;
use App\Imports\ProductsImport;
use App\Models\ProductModel;
use App\Http\Controllers\HelperController;
use App\Models\ProductImageModel;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;


class ProductController extends Controller
{

    private $images;
    private $product;
    public function __construct(ProductModel $product, ProductImageModel $images)
    {
        $this->product = $product;
        $this->images = $images;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = ProductResource::collection($this->product->list());
        $response = [
            'pagination' => [
                'total' => $datas->total(),
                'per_page' => $datas->perPage(),
                'current_page' => $datas->currentPage(),
                'last_page' => $datas->lastPage(),
                'from' => $datas->firstItem(),
                'to' => $datas->lastItem()
            ],
            'dataAll' => $datas,
        ];
        return response()->json($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductStoreRequest $request)
    {
        try {
            $product = $this->product->createProduct($request);
            if (count($request->images) > 0) {
                if ($product) {
                    foreach($request->images as $id_image){
                        $this->images->where('id',$id_image)->update([
                            "product_id" => $product->id
                        ]);
                    }
                }
            }
        }catch (\Exception){
            return response()->json(['message' => CREATE_ERROR ], 500);
        }
        return response()->json(['message' => CREATE_SUCCESS], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProductModel  $productModel
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $productInfor = new ProductResource($this->product->findOrFail($id)->load('productImages'));
        return response()->json($productInfor);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProductModel  $productModel
     * @return \Illuminate\Http\Response
     */
    public function update(ProductUpdateRequest $request, $id)
    {
        try {
            $productUpdate = $this->product->updateProduct($request, $id);
            if (count($request->images) > 0) {
                if ($productUpdate) {
                    foreach($request->images as $id_image){
                        $this->images->where('id',$id_image)->update([
                            "product_id" => $productUpdate->id
                        ]);
                    }
                }
            }
        }catch (\Exception){
            return response()->json(['message' => UPDATE_ERROR ], 500);
        }
        return response()->json(['message' => UPDATE_SUCCESS], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductModel  $productModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $product = $this->product->findOrFail($id);
            $product->delete();
        }catch (\Exception){
            return response()->json(['message' => DELETE_ERROR, 500]);
        }
        return response()->json(['message' => DELETE_SUCCESS, 200]);
    }

    public function import(Request $request)
    {
        try {
            Excel::import(new ProductsImport,$request->file);
        }catch (\Exception){
            return response()->json(["message" => "Upload fail!"], 500);
        }
        return response()->json(["message" => "Upload completed!"], 200);
    }
}
