<?php

namespace App\Http\Controllers\Api;

use App\Exports\ExportRequest;
use App\Http\Controllers\Controller;
use App\Http\Requests\RequestStoreRequest;
use App\Http\Requests\RequestUpdateRequest;
use App\Http\Resources\RequestResource;
use App\Imports\UpdateRequestImportExcel;
use App\Models\ProductModel;
use App\Models\RequestChangeProductModel;
use App\Models\RequestModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Maatwebsite\Excel\Facades\Excel;



class RequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $request;
    private $products;
    private $request_returns;

    public function __construct(RequestModel $requestModel, ProductModel $productModel, RequestChangeProductModel $request_returns)
    {
        $this->request= $requestModel;
        $this->products= $productModel;
        $this->request_returns= $request_returns;
    }
    public function index()
    {
        $datas = RequestResource::collection($this->request->listFindStatus(REQUESTED));
        $response = [
            'pagination' => [
                'total' => $datas->total(),
                'per_page' => $datas->perPage(),
                'current_page' => $datas->currentPage(),
                'last_page' => $datas->lastPage(),
                'from' => $datas->firstItem(),
                'to' => $datas->lastItem()
            ],
            'dataAll' => $datas,
        ];
        return response()->json($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestStoreRequest $request)
    {
        try {
            $this->request->createRequest($request);
        }catch (\Exception){
            return response()->json(['message' => CREATE_ERROR], 500);
        }
        return response()->json(['message' => CREATE_SUCCESS], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RequestModel  $requestModel
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $datas = $this->request->with(['user', 'category.products' => function($q) {
                $q->whereNull('request_id');
            }])->where('id', $id)->get();
        }catch (\Exception $e){
            return response()->json($e, 403);
        }
        return response()->json($datas, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\RequestModel  $requestModel
     * @return \Illuminate\Http\Response
     */
    public function update(RequestUpdateRequest $request, $id)
    {
        try {
            $this->request->updateRequest($request, $id);
        }catch (\Exception){
            return response()->json(['message' => UPDATE_ERROR], 500);
        }
        return response()->json(['message' => UPDATE_SUCCESS], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RequestModel  $requestModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->request->findOrFail($id)->delete();
        }catch (\Exception){
            return response()->json(['message' => DELETE_ERROR], 500);
        }
        return response()->json(['message' => DELETE_SUCCESS], 200);
    }

    public function approvedWhereList(Request $request){
        try {
            $arrApproveRequestId = [];
            foreach ($request->objectData as $item){
                if ($item["status"] == REQUESTED){
                    $arrApproveRequestId[] = $item["id"];
                }
                if ($item["status"] == APPROVED && count($item["products"]) < 1){
                    $arrApproveRequestId[] = $item["id"];
                }
            }
            $datas = $this->request->with(['user','category.products' => function($q) {
                $q->whereNull('request_id');
            }])->whereIn('id', $arrApproveRequestId)->get();
        }catch (\Exception $e){
            return response()->json($e, 403);
        }
        return response()->json($datas, 200);
    }

    public function approved(Request $request)
    {
        $this->request->updateStatus($request->objectData, APPROVED);
        return response()->json(['status' => true, 'message' => 'Yêu cầu đã được chấp nhận',]);
    }

    public function approvedList()
    {
        $datas = RequestResource::collection($this->request->listFindStatus(APPROVED));
        $response = [
            'pagination' => [
                'total' => $datas->total(),
                'per_page' => $datas->perPage(),
                'current_page' => $datas->currentPage(),
                'last_page' => $datas->lastPage(),
                'from' => $datas->firstItem(),
                'to' => $datas->lastItem()
            ],
            'dataAll' => $datas,
        ];
        return response()->json($response);
    }

    public function rejected(Request $request)
    {
        try {
            foreach ($request->data as $key => $value){
                $this->request->find($value["id"])->update([
                    "status" => REJECTED,
                    "message" => $request->reason[$key]
                ]);
            }
        }catch (\Exception){
            return response()->json(["message" => UPDATE_ERROR], 500);
        }
        return response()->json(["message" => UPDATE_SUCCESS], 200);
    }

    public function rejectedList()
    {
        $datas = RequestResource::collection($this->request->listFindStatus(REJECTED));
        $response = [
            'pagination' => [
                'total' => $datas->total(),
                'per_page' => $datas->perPage(),
                'current_page' => $datas->currentPage(),
                'last_page' => $datas->lastPage(),
                'from' => $datas->firstItem(),
                'to' => $datas->lastItem()
            ],
            'dataAll' => $datas,
        ];
        return response()->json($response);
    }

    public function requestAll()
    {
        $datas = RequestResource::collection($this->request->list());
        $response = [
            'pagination' => [
                'total' => $datas->total(),
                'per_page' => $datas->perPage(),
                'current_page' => $datas->currentPage(),
                'last_page' => $datas->lastPage(),
                'from' => $datas->firstItem(),
                'to' => $datas->lastItem()
            ],
            'dataAll' => $datas,
        ];
        return response()->json($response);
    }

    public function returntData()
    {
        $datas = $this->request_returns->listAll();
        $response = [
            'pagination' => [
                'total' => $datas->total(),
                'per_page' => $datas->perPage(),
                'current_page' => $datas->currentPage(),
                'last_page' => $datas->lastPage(),
                'from' => $datas->firstItem(),
                'to' => $datas->lastItem()
            ],
            'dataAll' => $datas,
        ];
        return response()->json($response);
    }

    public function returntDataProduct($id)
    {
        $datas = $this->products->with('productImages')->where("request_id", $id)->get();
        return response()->json($datas);
    }

    public function returnDataReject(Request $request){
        $requestReject = [];
        foreach ($request->objectData as $item){
            if ($item["status"] == 2){
                $requestReject[] = $this->request_returns->find($item["id"])->load('user','request.category');
            }
        }
        return response()->json($requestReject, 200);
    }

    public function returnted(Request $request)
    {
        if ($request->status == REJECT){
            $requestReject = [];
            foreach ($request->objectData as $key => $item){
                if ($item["status"] == 2){
                    $requestReject[$key]["id_request_return"] = $item["id"];
                    $requestReject[$key]["reason"] = $request->reason[$key];
                }
            }
            foreach ($requestReject as $item){
                $this->request_returns->find($item["id_request_return"])->update([
                    "status" => REJECT,
                    "reason" => $item["reason"]
                ]);
            }
        }else {
            foreach ($request->objectData as $item) {
                if ($request->user_id) {
                    if ($item["status"] == NOT_RETURN || $item["status"] == REJECT) {
                        $requestUser = $this->request->find($item["request_id"]);
                        if ($requestUser->user_id != $request->user_id) {
                            return response()->json(['message' => CREATE_ERROR], 403);
                        }
                        $this->request_returns->where("request_id", $item["request_id"])->update([
                            "status" => $request->status,
                            "reason" => null
                        ]);
                    }
                    if ($item["status"] == SUCCESS || $item["status"] == PENDING) {
                        continue;
                    }
                }
                if ($item["status"] == PENDING){
                    $this->request_returns->where("request_id", $item["request_id"])->update([
                        "status" => $request->status,
                        "reason" => null
                    ]);
                    if ($request->status == RETURNTED) {
                        $this->request->find($item["request"]["id"])->update([
                            "status" => $request->status
                        ]);
                        $this->products->where("request_id", $item["request_id"])->update([
                            "request_id" => null
                        ]);
                    }
                }
                if ($item["status"] == SUCCESS){
                    $statusRequestReturn = $this->request_returns->where("request_id", $item["request_id"])->update([
                        "status" => $request->status,
                        "reason" => null
                    ]);
                    if ($statusRequestReturn) {
                        if ($request->status == RETURNTED) {
                            $this->request->find($item["request"]["id"])->update([
                                "status" => $request->status
                            ]);
                        }
                        $this->products->where("request_id", $item["request_id"])->update([
                            "request_id" => null
                        ]);
                    }
                }
            }
        }
        return response()->json(["message" => CREATE_SUCCESS], 200);
    }

    public function myListAll()
    {
        $datas = $this->request->myListAll();
        $response = [
            'pagination' => [
                'total' => $datas->total(),
                'per_page' => $datas->perPage(),
                'current_page' => $datas->currentPage(),
                'last_page' => $datas->lastPage(),
                'from' => $datas->firstItem(),
                'to' => $datas->lastItem()
            ],
            'dataAll' => $datas,
        ];
        return response()->json($response);
    }

    public function myListPendding()
    {
        $datas = $this->request->myListStatus(REQUESTED);
        $response = [
            'pagination' => [
                'total' => $datas->total(),
                'per_page' => $datas->perPage(),
                'current_page' => $datas->currentPage(),
                'last_page' => $datas->lastPage(),
                'from' => $datas->firstItem(),
                'to' => $datas->lastItem()
            ],
            'dataAll' => $datas,
        ];
        return response()->json($response);
    }

    public function myUpdateRequest(RequestUpdateRequest $request, $id)
    {
        $productUpdate = $this->request->updateRequest($request, $id);
        if($productUpdate){
            return response()->json(['status' => true, 'message' => UPDATE_SUCCESS]);
        }
        return response()->json(['status' => false, 'message' => UPDATE_ERROR]);
    }

    public function myApprovedList()
    {
        $datas = $this->request->myListStatus(APPROVED);
        $response = [
            'pagination' => [
                'total' => $datas->total(),
                'per_page' => $datas->perPage(),
                'current_page' => $datas->currentPage(),
                'last_page' => $datas->lastPage(),
                'from' => $datas->firstItem(),
                'to' => $datas->lastItem()
            ],
            'dataAll' => $datas,
        ];
        return response()->json($response);
    }

    public function myRejectedList()
    {
        $datas = $this->request->myListStatus(REJECTED);
        $response = [
            'pagination' => [
                'total' => $datas->total(),
                'per_page' => $datas->perPage(),
                'current_page' => $datas->currentPage(),
                'last_page' => $datas->lastPage(),
                'from' => $datas->firstItem(),
                'to' => $datas->lastItem()
            ],
            'dataAll' => $datas,
        ];
        return response()->json($response);
    }

    public function export(Request $request)
    {
        if (! $request->hasValidSignature()) {
            return response()->json(['message' => 'Link không còn tồn tại']);
        }
        return Excel::download(new ExportRequest(), 'danh_sach_muon_'.time().'.xlsx');
    }

    public function exportCSV(Request $request)
    {
        return response()->json([
            'url' => URL::temporarySignedRoute('export.request', now()->addMinutes(1), $request->data)
        ]);
    }

    public function import(Request $request)
    {
        try {
            Excel::import(new UpdateRequestImportExcel(), $request->file);
        }catch (\Exception){
            return response()->json(["message" => "Upload fail!"], 500);
        }
        return response()->json(["message" => "Upload completed!"], 200);
    }

    public function updateStatusSingle(Request $request, $id)
    {
        try {
            $arrayRequests = [];
            foreach ($request->id as $key => $idProduct){
                $arrayRequests[$key]["product_id"] = $request->id_product[$key];
                $arrayRequests[$key]["id"] = $idProduct;
            }
            foreach ($arrayRequests as $items){
                $requestFind = $this->request->find($items["id"]);

                if (count($items["product_id"]) < 1){
                    return response()->json(['message' => 'Vui lòng chọn sản phẩm duyệt!'], 500);
                }
                if ($requestFind->quantity != count($items["product_id"])){
                    return response()->json(['message' => 'Một vài sản phẩm có số lượng mượn không đồng nhất với số lượng trong kho'], 500);
                }
                foreach ($items["product_id"] as $idProduct){
                    $productItem = $this->products->find($idProduct);
                    if (!$productItem->request_id) {
                        $this->products->find($idProduct)->update([
                            "request_id" => $items["id"]
                        ]);
                    }else {
                        return response()->json(['message' => UPDATE_ERROR], 500);
                    }
                }
                $updateRequestStatus = $requestFind->update(["status" => $request->status]);
                if ($updateRequestStatus){
                    $this->request_returns->create([
                        "request_id" => $requestFind->id,
                        "user_id" => $requestFind->user_id
                    ]);
                }
            }

        }catch (\Exception){
            return response()->json(['message' => UPDATE_ERROR], 500);
        }
        return response()->json(['message' => 'Yêu cầu đã được chấp nhận'], 200);
    }
}
