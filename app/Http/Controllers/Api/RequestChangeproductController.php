<?php

namespace App\Http\Controllers\Api;

use App\Models\RequestChangeProductModel;
use App\Http\Controllers\Controller;
use App\Http\Requests\RequestChangeRequest;
use Illuminate\Http\Request;

class RequestChangeproductController extends Controller
{
    private $changeRequest ;
    public function __construct(RequestChangeProductModel $changeRequest)
    {
        $this->changeRequest = $changeRequest;
    }

    public function createChangeProduct(RequestChangeRequest $request)
    {
        $this->changeRequest->createChange($request);
        return response()->json(['status' => true, 'message' => CREATE_SUCCESS]);
    }

    public function approvedChange($id)
    {
        $this->changeRequest->updateStatusChange($id, 1);
        return response()->json(['status' => true, 'message' => APPROVED]);
    }

    public function rejectedChange($id)
    {
        $this->changeRequest->updateStatusChange($id, 0);
        return response()->json(['status' => true, 'message' => REJECTED]);
    }
}
