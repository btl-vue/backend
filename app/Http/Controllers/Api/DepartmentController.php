<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\DepartmentStoreRequest;
use App\Http\Requests\DepartmentUpdateRequest;
use App\Http\Resources\DepartmentResource;
use App\Models\DepartmentModel;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    protected $department;
    public function __construct(DepartmentModel $department)
    {
        $this->department = $department;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = DepartmentResource::collection($this->department->list());
        $response = [
            'pagination' => [
                'total' => $datas->total(),
                'per_page' => $datas->perPage(),
                'current_page' => $datas->currentPage(),
                'last_page' => $datas->lastPage(),
                'from' => $datas->firstItem(),
                'to' => $datas->lastItem()
            ],
            'dataAll' => $datas,
        ];
        return response()->json($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DepartmentStoreRequest $request)
    {
        try {
            $this->department->createDepartment($request);
        }catch (\Exception){
            return response()->json(['message' => CREATE_ERROR], 500);
        }
        return response()->json(['message' => CREATE_SUCCESS], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $department = new DepartmentResource($this->department->findOrFail($id));
        return response()->json($department);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DepartmentUpdateRequest $request, $id)
    {
        try {
            $this->department->updateDepartment($request, $id);
        }catch (\Exception){
            return response()->json(['message' => UPDATE_ERROR], 500);
        }
        return response()->json(['message' => UPDATE_SUCCESS], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->department->find($id)->delete();
        }catch (\Exception){
            return response()->json(['message' => DELETE_ERROR], 500);
        }
        return response()->json(['message' => DELETE_SUCCESS], 200);
    }
}
