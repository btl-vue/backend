<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\CategoryModel;
use App\Models\DepartmentModel;
use App\Models\ProductModel;
use App\Models\RequestChangeProductModel;
use App\Models\RequestModel;
use App\Models\User;
use Illuminate\Http\Request;

class DataInfomationController extends Controller
{
    protected $departments;
    protected $categories;
    protected $products;
    protected $users;
    protected $requests;
    protected $return;

    public function __construct(
        DepartmentModel $departments,
        CategoryModel $categories,
        ProductModel $products,
        User $users,
        RequestModel $requests,
        RequestChangeProductModel $return
    )
    {
        $this->departments = $departments;
        $this->categories = $categories;
        $this->products = $products;
        $this->users = $users;
        $this->requests = $requests;
        $this->return = $return;
    }

    public function departments(){
        try {
            $datas = $this->departments->listAll();
        }catch (\Exception $e){
            return response()->json($e, 403);
        }
        return response()->json($datas, 200);
    }

    public function categories()
    {
        try {
            $datas = $this->categories->listAll();
        }catch (\Exception $e){
            return response()->json($e, 403);
        }
        return response()->json($datas, 200);
    }

    public function products()
    {
        try {
            $datas = $this->products->listAll();
        }catch (\Exception $e){
            return response()->json($e, 403);
        }
        return response()->json($datas, 200);
    }

    public function users()
    {
        try {
            $datas = $this->users->listAll();
        }catch (\Exception $e){
            return response()->json($e, 403);
        }
        return response()->json($datas, 200);
    }

    public function requests()
    {
        try {
            $datas = $this->requests->listAll();
        }catch (\Exception $e){
            return response()->json($e, 403);
        }
        return response()->json($datas, 200);
    }

    public function home()
    {
        try {
            $datas = [
                "pruduct_in_stock" => $this->products->whereNull("request_id")->get(),
                "pruduct_not_in_stock" => $this->products->whereNotNull("request_id")->get(),
                "request_pending" => $this->requests->with(['user','category','products.productImages'])->where("status", REQUESTED)->get(),
                "request_return_pending" => $this->return->with(['user','request.category','request.products.productImages'])->where("status", PENDING)->get(),
            ];
        }catch (\Exception $e){
            return response()->json($e, 403);
        }
        return response()->json($datas, 200);
    }
}
