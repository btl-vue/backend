<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryStoreRequest;
use App\Http\Resources\CategoryResource;
use App\Models\CategoryModel;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $category;
    public function __construct(CategoryModel $category)
    {
        $this->category = $category;
    }

    public function index()
    {
        $datas = CategoryResource::collection($this->category->list());
        $response = [
            'pagination' => [
                'total' => $datas->total(),
                'per_page' => $datas->perPage(),
                'current_page' => $datas->currentPage(),
                'last_page' => $datas->lastPage(),
                'from' => $datas->firstItem(),
                'to' => $datas->lastItem()
            ],
            'dataAll' => $datas,
        ];
        return response()->json($response);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryStoreRequest $request)
    {
        try {
            $this->category->createCategory($request);
        }catch (\Exception){
            return response()->json(['message' => CREATE_ERROR], 500);
        }
        return response()->json(['message' => CREATE_SUCCESS], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CategoryModel  $categoryModel
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category= new CategoryResource($this->category->find($id));
        return response()->json($category);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CategoryModel  $categoryModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $this->category->updateCategory($request, $id);
        }catch (\Exception){
            return response()->json(['message' => UPDATE_ERROR], 500);
        }
        return response()->json(['message' => UPDATE_SUCCESS]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CategoryModel  $categoryModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->category->deleteCategory($id);
        }catch (\Exception){
            return response()->json(['message' => DELETE_ERROR], 500);
        }
        return response()->json(['message' => DELETE_SUCCESS], 200);
    }
}
