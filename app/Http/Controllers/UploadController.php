<?php

namespace App\Http\Controllers;

use App\Models\ProductImageModel;
use Illuminate\Http\Request;

class UploadController extends Controller
{
    private $images;
    public function __construct(ProductImageModel $images)
    {
        $this->images = $images;
    }
    public function UploadProducts(Request $request)
    {
        $imageName = $request->image->getClientOriginalName();
        $request->image->move(public_path('images'), $imageName);
        $image_path = "/images/".$imageName;
        $imageUpload = $this->images->create([
            'image' => $image_path,
            'domain' => env('APP_URL')
        ]);
        return response()->json(["id_image" => $imageUpload->id, "link_image" => $image_path, "domain" => env('APP_URL')]);
    }

    public function UploadCategories(Request $request)
    {
        $imageName = $request->image->getClientOriginalName();
        $request->image->move(public_path('images'), $imageName);
        $image_path = "/images/".$imageName;
        return response()->json(["link_image" => $image_path, "domain" => env('APP_URL')]);
    }

    public function DeleteProducts(Request $request){
        $image = $this->images->where('id',$request->id)->first();
        $image->delete();
        $path=public_path().'/images/'.$image->image;
        if (file_exists($path)) {
            unlink($path);
        }
        return response()->json($image);
    }
}
