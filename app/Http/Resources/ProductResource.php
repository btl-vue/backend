<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'category_id' => $this->category_id,
            'code_producer' => $this->code_producer,
            'code_tfo' => $this->code_tfo,
            'description' => $this->description,
            'type' => $this->type,
            'guarantee_date' => $this->guarantee_date,
            'created_at' => $this->created_at,
            'category' => $this->category,
            'productImages' => $this->ProductImages
        ];
    }
}