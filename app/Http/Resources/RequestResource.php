<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RequestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'category_id' => $this->category_id,
            'quantity' => $this->quantity,
            'status' => $this->status,
            'reason' => $this->reason,
            'action' => $this->action,
            'date_start' => $this->date_start,
            'date_end' => $this->date_end,
            'action' => $this->action,
            'created_at' => $this->created_at,
            'user' => $this->user,
            'category' => $this->category,
            'products' => $this->products,
            "message" => $this->message
        ];
    }
}
