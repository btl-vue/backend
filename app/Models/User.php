<?php

namespace App\Models;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use PHPOpenSourceSaver\JWTAuth\Contracts\JWTSubject;
use Illuminate\Support\Facades\Mail;
use App\Mail\ResetPassword;
use App\Mail\ForgotPassword;

class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'status',
        'department_id',
        'role',
        'phone'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function department()
    {
        return $this->belongsTo(DepartmentModel::class,'department_id', 'id');
    }

     /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function requests()
    {
        return $this->hasMany(RequestModel::class, 'user_id', 'id');
    }

    public function scopeSearch($query)
    {
        if (request()->keyword) {
            $query = $query->where(function ($search){
                $search->where('name', 'like', '%' . request()->keyword . '%')
                    ->orWhere('email', 'like', '%' . request()->keyword . '%')
                    ->orWhere('phone', 'like', '%' . request()->keyword . '%');
            });
        }
        return $query;
    }

    public function scopeDepartment($query)
    {
        if ($department = request()->department) {
            $query = $query->where('department_id', $department );
        }
        return $query;
    }

    public function scopeRole($query)
    {
        if ($role = request()->role) {
            $query = $query->where('role',$role);
        }
        return $query;
    }

    public function scopeStatus($query)
    {
        if ($status = request()->status) {
            $query = $query->where('status', $status);
        }
        return $query;
    }
    public function list()
    {
        return User::search()->status()->department()->role()->orderby('id', 'desc')->paginate(10);
    }
    public function createUser($request)
    {
        return User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'status' =>  USER_ON,
            'department_id' => $request->department_id,
            'role' => $request->role,
            'phone' => $request->phone ,
        ]);
    }

    public function updateUser($request, $id)
    {
        return User::findOrFail($id)->update([
            'status' => $request->status,
            'department_id' => $request->department_id,
            'role' => $request->role,
        ]);
    }

    public function editProfile($request, $id)
    {
        static::findOrFail($id)->update([
            'name' => $request->name,
            'phone' => $request->phone
        ]);
        if($request->password) {
            $this->changePassword($request->password, $id);
        }
    }

    public function mailResetPassword($user, $password)
    {
        $mailable = new ResetPassword($user, $password);
        Mail::to($user->email)->queue($mailable);
    }

    public function mailForgotPassword($user, $password)
    {
        $mailable = new ForgotPassword($user, $password);
        Mail::to($user->email)->queue($mailable);
    }

    public function resetPassword($id)
    {
        $user = static::findOrFail($id);
        $password = Str::random(10);
        static::changePassword($password,$id);
        static::mailResetPassword($user , $password);
    }


    public function changePassword($password, $id){
        static::findOrFail($id)->update([
            'password' => Hash::make($password),
        ]);
    }

    public function forgotPassword($email)
    {
        $user = $this->checkMail($email);
        if(!empty($user)) {
            $password = Str::random(10);
            static::changePassword($password,$user->id);
            static::mailForgotPassword($user , $password);
            return [
                'status' => true,
                'message'=>'Password mới đã được gửi về email của bạn',
            ];
        }
        return [
            'status' => false,
            'message'=>'Email không tồn tại',
        ];


    }

    public function checkMail($email)
    {
        $user= static::where('email', $email)->first();
        return $user;
    }

    public function deleteUser($id)
    {
        if(static::where('id',$id)->delete()){
            return true;
        }
        return false;
    }

    public function listAll()
    {
        $datas = static::orderBy('id', 'desc')->get();
        return  $datas;
    }

}
