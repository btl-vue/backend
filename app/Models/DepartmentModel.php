<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DepartmentModel extends Model
{
    use HasFactory;

    protected $table = "departments";
    public $timestamps = false;
    protected $fillable = [
        'name',
        'phone'
    ];

    public function user()
    {
        return $this->hasMany(User::class,'department_id', 'id');
    }

    public function createDepartment($request)
    {
        return static::create([
            'name' => $request->name,
            'phone' => $request->phone
        ]);
    }

    public function updateDepartment($request, $id)
    {
        return static::find($id)->update([
            'name' => $request->name,
            'phone' => $request->phone
        ]);
    }

    public function scopeSearch($query)
    {
        if ($keyword = request()->keyword) {
            $query = $query->where('name', 'like', '%' . $keyword . '%');
        }
        return $query;
    }

    public function list()
    {
        $departments = static::search()->orderBy('id', 'desc')->paginate(10);
        return  $departments;
    }

    public function listAll()
    {
        $departments = static::orderBy('id', 'desc')->get();
        return  $departments;
    }

}
