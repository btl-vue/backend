<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RequestChangeProductModel extends Model
{
    use HasFactory;
    public $timestamps = true;
    protected $table = 'request_returns';
    protected $fillable = [
        'request_id',
        'user_id',
        'reason',
        'status',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function request()
    {
        return $this->belongsTo(RequestModel::class, 'request_id', 'id');
    }

    public function createChange($request)
    {
        static::create([
            'request_id' => $request->request_id,
        ]);
    }
    public function updateStatusChange($id, $status)
    {
        static::findOrFail($id)->update([
            'status' => $status
        ]);
    }

    public function scopeWhereUser($query)
    {
        if (request()->user) {
            $query = $query->where('user_id', request()->user);
        }
        return $query;
    }

    public function listAll()
    {
        $datas = static::with(['user','request.category','request.products.productImages'])->whereUser()->orderBy('id', 'desc')->paginate(10);
        return  $datas;
    }
}
