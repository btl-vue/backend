<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use App\Helper\Image;

class CategoryModel extends Model
{
    use HasFactory;
    protected $table = 'categories';
    protected $fillable = [
        'name',
        'slug',
        'image',
        'domain'
    ];
    const FOLDER = 'Categories';

    public function products()
    {
        return $this->hasMany(ProductModel::class, 'category_id', 'id');
    }

    public function requests()
    {
        return $this->hasMany(RequestModel::class, 'category_id', 'id');
    }

    public function scopeSearch($query)
    {
        if ($keyword = request()->keyword) {
            $query = $query->where('name', 'like', '%' . $keyword . '%');
        }
        return $query;
    }
    public function list()
    {
        $categories = static::with(['products'])->search()->orderBy('id', 'desc')->paginate(10);
        return  $categories;
    }

    public function createCategory($request)
    {
        $slug = Str::slug($request->name);
        static::create([
            'name' => $request->name,
            'slug' => $slug,
            'image' => $request->image,
            'domain' => env('APP_URL')
        ]);
    }

    public function updateCategory($request, $id)
    {
        $slug = Str::slug($request->name);
        $category = static::findOrFail($id);
        $category->update([
            'name' => $request->name,
            'slug' => $slug,
            'image' => $request->image
        ]);
    }

    public function deleteCategory($id)
    {
        if(static::where('id',$id)->delete()){
            return true;
        }
        return false;
    }

    public function listAll()
    {
        $categories = static::orderBy('id', 'desc')->get();
        return  $categories;
    }

}
