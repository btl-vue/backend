<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class RequestModel extends Model
{
    use HasFactory;
    public $timestamps = true;
    protected $table = 'requests';
    protected $fillable = [
        'user_id',
        'category_id',
        'quantity',
        'status',
        'reason',
        'message',
        'action',
        'date_start',
        'date_end'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function category()
    {
        return $this->belongsTo(CategoryModel::class, 'category_id', 'id');
    }
    public function products()
    {
        return $this->hasMany(ProductModel::class, 'request_id', 'id');
    }
    public function scopeSearch($query)
    {
        if ($keyword = request()->keyword) {
            $query = $query->where('name', 'like', '%' . $keyword . '%');
        }
        return $query;
    }

    public function scopeStatus($query)
    {
        if (request()->status != "") {
            $query = $query->where('status', request()->status);
        }
        return $query;
    }

    public function scopeWhereCategory($query)
    {
        if (request()->category != 0) {
            $query = $query->where('category_id', request()->category);
        }
        return $query;
    }

    public function scopeWhereUser($query)
    {
        if (request()->user_id) {
            $query = $query->where('user_id', request()->user_id);
        }
        return $query;
    }
    public function scopeWhereDate($query)
    {
        if (request()->date) {
            $start_date = isset(request()->date) ? date("Y-m-d", strtotime(request()->date[0])) :  "";
            $end_date = isset(request()->date) ? date("Y-m-d", strtotime(request()->date[1])) : "";
            if ($start_date && $end_date){
                $query = $query->where('date_start', ">=" ,$start_date)->where('date_end', "<=" ,$end_date);
            }
        }
        return $query;
    }


    public function list()
    {
        $requests = static::with(['user','category','products.productImages'])->whereDate()->whereUser()->whereCategory()->status()->orderBy('id','DESC')->paginate(10);
        return $requests;
    }

    public function listFindStatus($status)
    {
        $requests = static::with(['user','category','products.productImages'])->where('status', $status)->orderBy('id','DESC')->paginate(20);
        return $requests;
    }

    public function createRequest($requests)
    {
        static::create([
            'user_id' => $requests->user_id,
            'category_id' => $requests->category_id,
            'quantity' => $requests->quantity ?? 1,
            'status' => REQUESTED,
            'reason' => $requests->reason,
            'date_start' => date("Y-m-d", strtotime($requests->date[0])),
            'date_end' => date("Y-m-d", strtotime($requests->date[1])),
        ]);
    }

    public function updateRequest($requests, $id)
    {
        $request = static::findOrFail($id);
        if ($request->status == REQUESTED) {
            $request->update([
                'quantity' => $requests->quantity ?? 1,
                'reason' => $requests->reason ?? "",
                'date_start' => $requests->date_start ?? today(),
                'date_end' => $requests->date_end ?? today(),
            ]);
            return true;
        }
        return false;
    }

    public function updateStatus($listRequest, $status)
    {
        foreach ($listRequest as $values) {
            if($values['status'] == REQUESTED) {
                static::findOrFail($values['id'])->update([
                    'status' => $status
                ]);
            }
        }
    }

    public function updateAction($listRequest, $action)
    {
        foreach ($listRequest as $values) {
            if($values['status'] == APPROVED) {
                static::findOrFail($values['id'])->update([
                    'action' => $action
                ]);
            }
        }
    }

    public function myListAll()
    {
        $requests = static::with(['user','category'])->where('id',Auth::id())->status()->orderBy('id','DESC')->paginate(10);
        return $requests;
    }

    public function myListStatus($status)
    {
        $requests = static::with(['user','category'])->where('status', $status)->where('user_id',1)->status()->orderBy('id','DESC')->paginate(10);
        return $requests;
    }
    public function listAll()
    {
        $datas = static::with(['user','category','products.productImages'])->orderBy('id', 'desc')->get();
        return  $datas;
    }

    public function listExportExcel()
    {
        $datas = static::with(['user','category'])->orderBy('id', 'desc')->get();
        return  $datas;
    }

}
