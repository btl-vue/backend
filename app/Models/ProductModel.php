<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use App\Helper\Image;

class ProductModel extends Model
{
    use HasFactory;
    protected $table = "products";
    protected $fillable = [
        'name',
        'category_id',
        'code_producer',
        'code_tfo',
        'description',
        'type',
        'request_id',
        'guarantee_date'
    ];
    public $timestamp = false;
    const FOLDER = 'Products';

    public function category()
    {
        return $this->belongsTo(CategoryModel::class, 'category_id', 'id');
    }

    public function productImages()
    {
        return $this->hasMany(ProductImageModel::class, 'product_id', 'id');
    }

    public function scopeSearch($query)
    {
        if ($search = request()->keyword) {
            $query = $query->where('name', 'like', '%' . $search . '%')->orWhere('code_producer', 'like', '%' . $search . '%')
            ->orWhere('code_tfo', 'like', '%' . $search . '%');
        }
        return $query;
    }


    public function scopeCategory($query)
    {
        if(isset(request()->category) && request()->category !=null) {
            $query = $query->where('category_id',request()->category);
        }
        return $query;
    }

    public function scopeStatus($query)
    {
        if(isset(request()->status) && request()->status !=null) {
            $query = $query->where('status',request()->status);
        }
        return $query;
    }

    public function list()
    {
        $products = static::search()->category()->status()->with('category', 'productImages')->orderBy('id','DESC')->paginate(10);
        return  $products;
    }

    public function createProduct($request)
    {
        $product = static::create([
            'name' => $request->name,
            'category_id' => $request->category_id,
            'code_producer' => $request->code_producer,
            'code_tfo' => $request->code_tfo,
            'description' => $request->description,
            'type' => $request->type,
            'guarantee_date' => $request->guarantee_date
        ]);
        return $product;
    }

    public function updateProduct($request, $id)
    {
        $product = static::findOrFail($id);
        $product->update([
            'name' => $request->name,
            'category_id' => $request->category_id,
            'code_producer' => $request->code_producer,
            'code_tfo' => $request->code_tfo,
            'description' => $request->description,
            'type' => $request->type,
            'guarantee_date' => $request->guarantee_date
        ]);
        return $product;
    }

    public function listAll()
    {
        $datas = static::orderBy('id', 'desc')->get();
        return  $datas;
    }

}
