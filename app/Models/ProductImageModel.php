<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductImageModel extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'image_products';
    protected $fillable = [
        'product_id',
        'image',
        'domain'
    ];

    public function product ()
    {
        return $this->belongsTo(ProductModel::class, 'product_id', 'id');
    }
}
