<?php
namespace App\Helper;

class Image
{
    // $fodel: nơi lưu ảnh
    // $Slug: lấy tên ảnh theo slug truyền vào
    // $File : file ảnh 
    public function uploadImage($file, $folder, $slug=""){
        if($file){
            $path = 'uploads/'.$folder.'/';
            $newName = $slug .'-'. time() .'.'.$file->getClientOriginalExtension();
            $file->move($path, $newName);
            return $newName;
        }
        return redirect()->back()->with('erros','Lỗi chọn ảnh');
    }
   
    public function updateImage($file, $folder, $slug="",$oldName, $count=1 ){
        if($count <= 1) {
            $this->delImage($folder,$oldName);
        }
        $data= $this->uploadImage( $file, $folder, $slug);
        return $data;
    }
    
    public function delImage($folder,$name){
        if (!empty($name)) {
            $pathDelete = 'uploads/'.$folder.'/' . $name;
            if (file_exists($pathDelete)) {
                unlink($pathDelete); 
            }
        }
    }

    // public function ImageList($images, $folder,$id,$slug){
    //     $nameImages = '';
    //     if ($images) {
    //         foreach ($images as $imageItem) {
    //             if ($nameImages != null) {
    //                 $nameImages = $nameImages . '!' . $this->UploadImage($imageItem, $folder, $id, $slug);
    //             } else {
    //                 $nameImages = $this->UploadImage($imageItem, $folder,$id, $slug);
    //             }
    //         }
    //     }
    //     return $nameImages;
    // }
}
