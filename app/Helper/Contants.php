<?php
// Define message

define("CREATE_SUCCESS", "Create complete");
define("UPDATE_SUCCESS", "Update complete");
define("DELETE_SUCCESS", "Delete complete");
define("CREATE_ERROR", "Some error has occurred, please try again!");
define("UPDATE_ERROR", "Some error has occurred, please try again!");
define("DELETE_ERROR", "Some error has occurred, please try again!");


// status user
define("USER_ON", 0);
define("USER_OFF", 1);

// request status
define("REQUESTED", 0);
define("APPROVED", 1);
define("REJECTED", 2);
define("RETURNTED", 3);


// request action
define("NOT_RETURN", 1);
define("PENDING", 2);
define("SUCCESS", 3);
define("REJECT", 4);


define("DIVISON_MANAGER", 0);
define("STORE_KEEPER", 1);
define("USER", 2);

// type product
define("PRODUCT_ON", 0);
define("PRODUCT_OFF", 1);







