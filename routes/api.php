<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\DepartmentController;
use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\RequestController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HelperController;
use GuzzleHttp\Middleware;
use App\Http\Controllers\UploadController;
use App\Http\Controllers\Api\DataInfomationController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('datas')->group(function (){
    Route::get('departments', [DataInfomationController::class, 'departments']);
    Route::get('categories', [DataInfomationController::class, 'categories']);
    Route::get('users', [DataInfomationController::class, 'users']);
    Route::get('products', [DataInfomationController::class, 'products']);
    Route::get('requests', [DataInfomationController::class, 'requests']);
    Route::get('home', [DataInfomationController::class, 'home']);

});

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::controller(AuthController::class)->group(function () {
    Route::post('login', 'login');
    Route::post('register', 'register');
    Route::post('logout', 'logout');
    Route::post('refresh', 'refresh');
});
Route::get('helper/products', [HelperController::class, 'products']);
Route::get('helper/users', [HelperController::class, 'users']);
Route::get('helper/requests', [HelperController::class, 'requests']);

// Route::middleware(['auth'])->group(function () {
    Route::put('users/update-profile', [UserController::class, 'updateProfile']);
    Route::put('users/forgot-password', [UserController::class, 'forgotPassword']);
    // Route::middleware(["check.role:".STORE_KEEPER.",".DIVISON_MANAGER])->group(function () {
        Route::get('users/reset-password/{id}', [UserController::class, 'resetPassword']);
        Route::post('upload-image-categories', [UploadController::class, 'UploadCategories']);
        Route::post('upload-image-product', [UploadController::class, 'UploadProducts']);
        Route::post('delete-image-product', [UploadController::class, 'DeleteProducts']);
        Route::post('upload-excel', [ProductController::class, 'import']);
        Route::post('upload-excel-request', [RequestController::class, 'import']);
        Route::post('requests/all', [RequestController::class, 'requestAll']);
        Route::get('requests/approved', [RequestController::class, 'approvedList']);
        Route::get('requests/rejected', [RequestController::class, 'rejectedList']);
        Route::put('requests/approved', [RequestController::class, 'approved']);
        Route::put('requests/rejected', [RequestController::class, 'rejected']);
        Route::get('requests/returnt-data', [RequestController::class, 'returntData']);
        Route::get('requests/return-data-product/{id}', [RequestController::class, 'returntDataProduct']);
        Route::post('requests/returnt-data-reject', [RequestController::class, 'returnDataReject']);
        Route::post('requests/returnted', [RequestController::class, 'returnted']);
        Route::post('requests/export-url', [RequestController::class, 'exportCSV'])->name('export.url');
        Route::get('requests/export', [RequestController::class, 'export'])->name('export.request');
        Route::apiResource('categories', CategoryController::class);
        Route::apiResource('departments', DepartmentController::class);
        Route::apiResource('products', ProductController::class);
        Route::apiResource('users', UserController::class);
    // });
    // danh sach request cua user
    Route::get('my-requests/all', [RequestController::class, 'myListAll']);
    Route::get('my-requests/pendding', [RequestController::class, 'myListPendding']);
    Route::put('my-requests/{id}', [RequestController::class, 'myUpdateRequest']);
    Route::get('my-requests/approved', [RequestController::class, 'myApprovedList']);
    Route::get('my-requests/rejected', [RequestController::class, 'myRejectedList']);
    Route::put('my-requests/update-status-requests/{id}', [RequestController::class, 'updateStatusSingle']);
    Route::apiResource('requests', RequestController::class);
    Route::post('approved-request-multiple', [RequestController::class, 'approvedWhereList']);
// });
